// Inicio de la solución
class PrecioTotal{
    // Atributos
    double totalPrecios = 0;
    double totalNacional = 0;
    double totalUrbano = 0;
    Flete[] flete;
    // Constructores
    public PrecioTotal(Flete[] flete){
        //this.flete = new Flete[];
        this.flete = flete;
    }
    // Metodos
    public void calcularTotales() {
        for(int i=0;i<flete.length;i++){
            if(flete[i].toString().startsWith("Nacional")){
                Nacional strNacional = (Nacional) flete[i];
                totalNacional += strNacional.calcularPrecio(strNacional.getPeso(),strNacional.getTamanio(),strNacional.getPrecioBase());
            }else{
                if(flete[i].toString().startsWith("Urbano")){
                    Urbano strUrbano = (Urbano) flete[i];
                    totalUrbano += strUrbano.calcularPrecio(strUrbano.getPeso(),strUrbano.getTamanio(),strUrbano.getPrecioBase());
                }
            }  
        }
        totalPrecios = totalNacional + totalUrbano;
        
    }
    public void mostrarTotales() {
        // Calculo de totales
        calcularTotales();
        System.out.println("Total Fletes " + totalPrecios);
        System.out.println("Total Nacional " + totalNacional);
        System.out.println("Total Urbano " + totalUrbano);
    }
}

class Flete {
    // Constantes
    private static final double PESO = 10.0;
    private static final double TAMANIO = 4.5;
    private static final double PRECIO_BASE = 1000.0;
    // Atributos
    private double peso;
    private double tamanio;
    private double precioBase;
    // Constructores
    public Flete(double peso,double tamanio){
        this.peso = peso;
        this.tamanio = tamanio;
        this.precioBase = PRECIO_BASE;
    }
    public Flete(double precioBase){
        this.peso = PESO;
        this.tamanio = TAMANIO;
        this.precioBase = precioBase;
    }
    public Flete(){
        this.peso = PESO;
        this.tamanio = TAMANIO;
        this.precioBase = PRECIO_BASE;
    }
    // Metodos
    public double calcularPrecio(){
        return 0.0;
    }
    // getters/setters de ser necesarios
    public double getPeso() {
        return this.peso;
    }
    public void setPeso(double peso) {
        this.peso = peso;
    }
    public double getTamanio() {
        return this.tamanio;
    }
    public void setTamanio(double tamanio) {
        this.tamanio = tamanio;
    }
    public double getPrecioBase() {
        return this.precioBase;
    }
    public void setPrecioBase(double precioBase) {
        this.precioBase = precioBase;
    }
    
}

class Nacional extends Flete {
    // Constantes
    private static final double CAPACIDAD = 8.0;
    // Constructores
    public Nacional(double peso,double tamanio){
        super(peso, tamanio);
    }
    public Nacional(double precioBase){
        super(precioBase);
    }
    public Nacional(){
        super();
    }
    // Metodos
    public double calcularPrecio(double peso,double tamanio,double precioBase){
        double precioFinal;
        // Calculos
        precioFinal = peso * tamanio * CAPACIDAD;
        precioFinal += precioBase;
        return precioFinal;
    }
    // getters/setters de ser necesarios
    
}

class Urbano extends Flete {
    // Constantes
    private final static int TIEMPO = 2;
    // Constructores
    public Urbano(double peso,double tamanio){
        super(peso, tamanio);
    }
    public Urbano(double precioBase){
        super(precioBase);
    }
    public Urbano(){
        super();
    }
    // Metodos
    public double calcularPrecio(double peso,double tamanio,double precioBase){
        double precioFinal;
        // Calculos
        precioFinal = peso * tamanio * TIEMPO;
        precioFinal += precioBase;
        return precioFinal;
    }
    // getters/setters de ser necesarios
}
// Fin de la solución
    


public class App {
    public static void main(String[] args) {
        // Caso de Prueba 1
        Flete flete[] = new Flete[5];
        flete[0] = new Nacional(100.0, 10.0);
        flete[1] = new Nacional(200);
        flete[2] = new Urbano(150, 20.0);
        flete[3] = new Urbano();
        flete[4] = new Nacional();
        PrecioTotal solucion = new PrecioTotal(flete);
        solucion.mostrarTotales();
            }
}
